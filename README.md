# Lab 8 - Exploratory testing and UI

[![pipeline status](https://gitlab.com/danmory/s23-lab8-exploratory-testing-and-ui-testing/badges/master/pipeline.svg)](https://gitlab.com/danmory/s23-lab8-exploratory-testing-and-ui-testing/-/commits/master)

## Description

Exploratory tests for <https://go.dev>.

All test cases are in *TESTCASES.md*. UI test is in *test.py*.
