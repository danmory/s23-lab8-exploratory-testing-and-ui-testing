import pytest
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By

@pytest.fixture
def browser():
  chrome_options = webdriver.ChromeOptions()
  chrome_options.add_argument('--headless')
  chrome_options.add_argument('--no-sandbox')
  chrome_options.add_argument('--disable-dev-shm-usage')
  
  chrome = webdriver.Remote(
      command_executor='http://selenium__standalone-chrome:4444/wd/hub',
      desired_capabilities=chrome_options.to_capabilities()
  )
  yield chrome
  chrome.quit()
  

def test_searching_go_packages(browser: WebDriver):
  browser.get("https://go.dev/")
  
  browser.find_element(By.LINK_TEXT, "Packages").click()
  assert "pkg.go.dev" in browser.current_url
  
  
  search_input = browser.find_element(By.CSS_SELECTOR, "input[type='search']#AutoComplete")
  search_input.send_keys("some_invalid_module_name")
  search_btn = browser.find_element(By.CSS_SELECTOR, "button[type='submit']")
  search_btn.click()
  assert " 0 modules" in browser.find_element(By.CLASS_NAME, "SearchResults-summary").text
  
  search_input = browser.find_element(By.CSS_SELECTOR, "main input[type='search']")
  search_input.clear()
  search_input.send_keys("fmt")
  search_btn = browser.find_element(By.CSS_SELECTOR, "main button")
  search_btn.click()
  assert " 0 modules" not in browser.find_element(By.CLASS_NAME, "SearchResults-summary").text
  
  browser.find_element(By.CSS_SELECTOR, ".SearchSnippet:first-of-type .SearchSnippet-headerContainer a").click()
  assert "https://pkg.go.dev/fmt" == browser.current_url
