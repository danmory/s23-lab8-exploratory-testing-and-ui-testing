# Test cases

## Searching Go packages

| №  | What Done                                                          | Status | Comment |
|----|--------------------------------------------------------------------|--------|---------|
| 1  | Click "Packages" link in navigation bar                            | +      |         |
| 2  | Redirect to pkg.go.dev with search input field should be performed | +      |         |
| 3  | Put non-existing package name to the input                         | +      |         |
| 4  | Click "Search"                                                     | +      |         |
| 5  | No modules should be found                                         | +      |         |
| 6  | Replace search string with an existing package name(fmt)           | +      |         |
| 7  | Click "Search"                                                     | +      |         |
| 8  | Received list of found packages should be non-empty                | +      |         |
| 9  | Click on first entry                                               | +      |         |
| 10 | pkg.go.dev/{package_name} page should be openned                   | +      |         |

## Go playground

| №  | What Done                                                            | Status | Comment |
|----|----------------------------------------------------------------------|--------|---------|
| 1  | Navigate to page footer                                              | +      |         |
| 2  | Click "Playground" in "Get Started" section                          | +      |         |
| 3  | Go playground with HelloWorld program should be opened               | +      |         |
| 4  | Click "Run"                                                          | +      |         |
| 5  | Program output("Hello ...") should be printed below the code section | +      |         |
| 6  | Write random string instead of valid HelloWorld Go program           | +      |         |
| 7  | Click "Run"                                                          | +      |         |
| 8  | Message about build failure should appear below the code section     | +      |         |

## Key features of GO for Web Development

| №  | What Done                                                   | Status | Comment |
|----|-------------------------------------------------------------|--------|---------|
| 1  | Click "Why Go" link in navigation bar                       | +      |         |
| 2  | Dropdown menu should appear                                 | +      |         |
| 3  | Click "Use Cases" link in the menu                          | +      |         |
| 4  | Page with Go Use Cases should be opened                     | +      |         |
| 5  | Web Development should be among Use Cases                   | +      |         |
| 6  | Click "Learn More" in Web Development Use Case              | +      |         |
| 7  | Detailed information about Web Development should be opened | +      |         |
| 8  | "Key benefits" section should be present on the page        | +      |         |
